﻿using System;
namespace M17_UF1_E1_MyVector_SanchezCarlo
{
    class MyVector  
    {
        int[] inici = new int[2];
        int[] fi = new int[2];

        public MyVector(int[] inici, int[] fi)
        {
            this.inici = inici;
            this.fi = fi;
        }

        public int[] getInici()
        {
            return inici;
        }

        public int[] getFi()
        {
            return fi;
        }

        public void setInici(int[] inici)
        {
            this.inici = inici;
        }

        public void setFi(int[] fi)
        {
            this.fi = fi;
        }

        public double distance()
        {
            double u1 = (fi[0]) - (inici[0]);
            double u2 = (fi[1]) - (inici[1]);

            double result;
            result = Math.Abs(Math.Sqrt((Math.Pow(u1, 2)) + (Math.Pow(u2, 2))));

            return result;
        }

        public double fromOrigin()
        {
            int[] origin = new int[] { 0, 0 };
            MyVector v1 = new MyVector(origin, inici);
            MyVector v2 = new MyVector(origin, fi);

            if (v1.distance() <= v2.distance())
            {
                return v1.distance();
            } else
            {
                return v2.distance();
            }
        }

        public void inverse()
        {
            int[] inici = this.inici;
            int[] fi = this.fi;

            this.inici = fi;
            this.fi = inici;
        }   

        public String toString()
        {
            int x1 = inici[0];
            int y1 = inici[1];
            int x2 = fi[0];
            int y2 = fi[1];

            String text = String.Format("({0},{1}),({2},{3})", x1, y1, x2, y2);

            return text;
        }
    }
}