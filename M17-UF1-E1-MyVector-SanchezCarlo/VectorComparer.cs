﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace M17_UF1_E1_MyVector_SanchezCarlo
{
    class VectorComparer : IComparer<MyVector>
    {
        private bool method;

        public VectorComparer(bool method)
        {
            this.method = method;
        }

        public int Compare(MyVector a, MyVector b)
        {
            switch (method)
            {
                //Sort by distance
                case true:
                    if (a.distance() > b.distance())
                    {
                        return 1;
                    } else if (a.distance() < b.distance())
                    {
                        return -1;
                    } else
                    {
                        return 0;
                    }
                //Sort by proximity to (0,0)
                case false:
                    if (a.fromOrigin() > b.fromOrigin())
                    {
                        return 1;
                    }
                    else if (a.fromOrigin() < b.fromOrigin())
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
            }
        }
    }
}
