﻿using System;

namespace M17_UF1_E1_MyVector_SanchezCarlo
{

    class Program
    {
        static void Main(String[] args)
        {
            bool operation = true;
            while (operation)
            {
                Console.Write("\n******************\nIntrodueix l'opció:\n1. Hello {name}\n2. VectorGame\n0. Exit\n******************\n");
                int option;
                option = int.Parse(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        HelloName();
                        break;
                    case 2:
                        VectorGame();
                        break;
                    case 0:
                        operation = false;
                        break;
                    default:
                        operation = true;
                        break;
                }
            }
        }

        static void HelloName()
        {
            String name;
            Console.Write("Escribe tu nombre: ");
            name = Console.ReadLine();
            Console.WriteLine("Hola {0}!", name);
        }


        static void VectorGame()
        {
            Console.Write("\n******************\nFunció a executar:\n1. Random Vectors\n2. Sort Vectors\n******************\n");
            int option;
            option = int.Parse(Console.ReadLine());
            switch (option)
            {
                case 1:
                    RandomVectors();
                    break;
                case 2:
                    SortVectors();
                    break;
                default:
                    break;
            }
        }

        static void RandomVectors()
        {
            Console.Write("Llargaria del array:");
            int length = int.Parse(Console.ReadLine());
            MyVector[] array = new VectorGame().randomVectors(length);
            foreach (MyVector v in array)
            {
                Console.WriteLine("{0}", v.toString());
            }
        }

        static void SortVectors()
        {
            Console.Write("\n******************\nTipus de sort:\n1. Per distància\n2. Per proximitat al origen\n******************\n");
            int option;
            option = int.Parse(Console.ReadLine());
            bool method;
            switch (option)
            {
                case 1:
                    method = true;
                    break;
                case 2:
                    method = false;
                    break;
                default:
                    method = true;
                    break;
            }

            if (method)
            {
                MyVector[] array = new VectorGame().randomVectors(5);
                Console.WriteLine("Array original:");
                foreach (MyVector v in array)
                {
                    Console.WriteLine("{0} - Distancia: {1}", v.toString(), v.distance());
                }

                MyVector[] sortedArray = new VectorGame().SortVectors(array, method);
                Console.WriteLine("Array endreçat:");
                foreach (MyVector v in sortedArray)
                {
                    Console.WriteLine("{0} - Distancia: {1}", v.toString(), v.distance());
                }
            } else
            {
                MyVector[] array = new VectorGame().randomVectors(5);
                Console.WriteLine("Array original:");
                foreach (MyVector v in array)
                {
                    Console.WriteLine("{0} - Proximitat al origen: {1}", v.toString(), v.fromOrigin());
                }

                MyVector[] sortedArray = new VectorGame().SortVectors(array, method);
                Console.WriteLine("Array endreçat:");
                foreach (MyVector v in sortedArray)
                {
                    Console.WriteLine("{0} - Proximitat al origen: {1}", v.toString(), v.fromOrigin());
                }
            }
        }
    }
}
