﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_SanchezCarlo
{
    class VectorGame
    {
        public MyVector[] randomVectors(int length)
        {
            Random rnd = new Random();
            MyVector[] array = new MyVector[length];
            for (int i = 0; i <= array.Length - 1; ++i)
            {
                int x1 = rnd.Next(-100, 100);
                int y1 = rnd.Next(-100, 100);
                int x2 = rnd.Next(-100, 100);
                int y2 = rnd.Next(-100, 100);

                int[] inici = new int[] { x1, y1 };
                int[] fi = new int[] { x2, y2 };

                array[i] = new MyVector(inici, fi);
            }

            return array;
        }

        public MyVector[] SortVectors(MyVector[] array, bool method)
        {
            VectorComparer comparer = new VectorComparer(method);
            Array.Sort(array, comparer);
            
            return array;
        }
    }
}
